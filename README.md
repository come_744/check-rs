# Check

Convenience `assert!`-like macros which immediately return `None` or `Err(...)`
instead of panicking.

In a function returning an `Option<T>`, invoke the macro with just enough
parameters to get a condition to check.

```rust
check!(a < n);
check_eq!(a, b);
```

This will expand to:

```rust
if !(a < n) {
  return None;
}
if a != b {
  return None;
}
```

In a function returning a `Result<T, E>`, invoke the macro with an extra
argument, which is the error to return if the check fails (and must have type
`E`), just like you can add arguments to choose a panic message with `assert!`.

```rust
check!(a < n, MyError::TooBig);
check_eq!(a, b, MyError::NotEqual);
```

This will expand to:

```rust
if !(a < n) {
  return Err(MyError::TooBig);
}
if a != b {
  return Err(MyError::NotEqual);
}
```
